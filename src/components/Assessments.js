import React, { Component } from "react";
import DegreeInfo from "../data/assessments.json"; //import degree info from degree json file into DegreeInfo
import { Button, CardBody, Card, UncontrolledCollapse } from "reactstrap";

class Assessments extends Component {
	render() {
		return (
			<section id="viewAssessments" className="container mb-5">
				<div className="row justify-content-center">
					<h2 className="titles col-8 mb-3 text-center">Assessment List</h2>
					<div className="col-8 mb-4">
						{/* map the imported json data to view degree information */}
						{DegreeInfo.map((degreeDetail, i) => {
							return (
								// set unique key per item via props
								<div key={i}>
									<Button
										className="togbtn mb-2"
										size="lg"
										color="info"
										block
										id={"toggler" + i} // use unique key/index to target correct toggle otherwise all work at once
									>
										{degreeDetail.Num} - {degreeDetail.Title}
									</Button>
									<UncontrolledCollapse
										toggler={"toggler" + i} //toggled container target via key/index
										className="mb-2"
									>
										<Card>
											<CardBody className="row justify-content-center">
												<ul className="list-group col-12 col-sm-8">
													<div>
														<li className="list-group-item d-flex justify-content-between">
															<strong>Learning Outcomes:</strong>
															{degreeDetail.Learning_outcomes + ""}
														</li>
														<li className="list-group-item d-flex justify-content-between">
															<strong>Volume:</strong> {degreeDetail.Volume}
														</li>
														<li className="list-group-item d-flex justify-content-between">
															<strong>Weight: </strong> {degreeDetail.Weight}
														</li>
														<li className="list-group-item d-flex justify-content-between">
															<strong>Submission Date: </strong>{" "}
															{degreeDetail.Sub_date}
														</li>
													</div>
												</ul>
											</CardBody>
										</Card>
									</UncontrolledCollapse>
								</div>
							);
						})}
					</div>
				</div>
			</section>
		);
	}
}

export default Assessments;
