import React, { Component } from "react";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom"; //Routing imports for navigating between components
import { Form, Input } from "reactstrap"; //Import React bootstrap for styling

//Import components for routing from file structure

import Degrees from "./Degrees";
import Modules from "./Modules";
import Assessments from "./Assessments";
import CreateDegree from "./forms/CreateDegree";
import CreateModule from "./forms/CreateModule";
import CreateAssessment from "./forms/CreateAssessment";
import SignIn from "./forms/SignIn";
import Schedule from "./forms/Schedule";

class Navigation extends Component {
	render() {
		return (
			// Router used for application navigation
			<Router>
				<nav className="navbar navbar-expand-md navbar-dark">
					<button
						className="navbar-toggler"
						type="button"
						data-toggle="collapse"
						data-target="#navbarTogglerDemo03"
						aria-controls="navbarTogglerDemo03"
						aria-expanded="false"
						aria-label="Toggle navigation"
					>
						<span className="navbar-toggler-icon"></span>
					</button>
					<a className="navbar-brand" href="#">
						Edge Hill University
					</a>
					<div className="collapse navbar-collapse" id="navbarTogglerDemo03">
						<ul className="navbar-nav mr-auto mt-2 mt-lg-0">
							<li className="navlk nav-item mx-2 mb-0">
								<NavLink to="/signIn" className="text-decoration-none">
									Sign In
								</NavLink>
							</li>
							<li className="navlk nav-item mx-3">
								<NavLink to="/schedule" className="text-decoration-none">
									Schedule Time-Slots
								</NavLink>
							</li>
						</ul>
						<Form className="form-inline my-2 my-lg-0">
							<Input
								className="form-control mr-sm-2"
								bsSize="sm"
								type="search"
								placeholder="Search.."
								aria-label="Search"
							></Input>
							<a
								className="btn btn-info btn-sm my-2 my-sm-0"
								href="/"
								size="sm"
								type="submit"
							>
								Search
							</a>
						</Form>
					</div>
				</nav>
				<div className="jumbotron jumbotron-fluid py-3 mb-3">
					<div className="container text-center">
						<h1 className="display-5">
							Academic Management and Tracking Application
						</h1>
						<p className="lead"></p>
					</div>
				</div>
				<div className="container-fluid">
					<div className="row justify-content-around d-flex">
						<div className="col-8 col-md-6 col-lg-4 text-center mb-4">
							<div className="navCard card">
								<div className="card-body shadow-sm py-2">
									<h3 className="card-title">Degree Programmes</h3>
									<div>
										<NavLink
											to="/degrees"
											className="btn btn-sm btn-info shadow-sm m-1"
										>
											View Degrees
										</NavLink>
									</div>

									<div>
										<NavLink
											to="/createDegree"
											className="btn btn-sm btn-info shadow-sm m-1"
										>
											Create Degree
										</NavLink>
									</div>
								</div>
							</div>
						</div>
						<div className="col-8 col-md-6 col-lg-4 text-center mb-2">
							<div className="navCard card">
								<div className="card-body shadow-sm py-2">
									<h3 className="card-title">Modules</h3>
									<div>
										<NavLink
											to="/modules"
											className="btn btn-sm btn-info shadow-sm m-1"
										>
											View Modules
										</NavLink>
									</div>
									<div>
										<NavLink
											to="/createModule"
											className="btn btn-sm btn-info shadow-sm m-1"
										>
											Create Module
										</NavLink>
									</div>
								</div>
							</div>
						</div>
						<div className="col-8 col-md-6 col-lg-4 text-center mb-2">
							<div className="navCard card">
								<div className="card-body shadow-sm py-2">
									<h3 className="card-title">Assessments</h3>
									<div>
										<NavLink
											to="/assessments"
											className="btn btn-sm btn-info shadow-sm m-1"
										>
											View Assessments
										</NavLink>
									</div>

									<div>
										<NavLink
											to="/createAssessment"
											className="btn btn-sm btn-info shadow-sm m-1"
										>
											Create Assessment
										</NavLink>
									</div>
								</div>
							</div>
						</div>
						<nav className="navbar fixed-bottom align-items-center d-none d-md-inline-flex">
							<p className="my-2 text-white px-0 mx-sm-1">
								{" "}
								Wade Phillips &copy; 2020. All rights reserved
							</p>
							<p className="my-2 text-white px-0 mx-sm-1">
								CIS2169 CW02 - Academic Tracking Application
							</p>
						</nav>

						{/* Route paths defined to components for navigation within application */}
						<Route path="/signIn" component={SignIn} />
						<Route path="/schedule" component={Schedule} />
						<Route path="/createDegree" component={CreateDegree} />
						<Route path="/createModule" component={CreateModule} />
						<Route path="/createAssessment" component={CreateAssessment} />
						<Route path="/degrees" component={Degrees} />
						<Route path="/modules" component={Modules} />
						<Route path="/assessments" component={Assessments} />
					</div>
				</div>
			</Router>
		);
	}
}

export default Navigation;
