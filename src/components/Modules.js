import React, { Component } from "react";
import { UncontrolledCollapse, Button, CardBody, Card } from "reactstrap";
import ModuleInfo from "../data/modules.json";

class Modules extends Component {
	render() {
		return (
			<section id="viewModules" className="container mb-5">
				<div className="row justify-content-center">
					<h2 className="titles col-8 mb-3 text-center">Module List</h2>
					<div className="col-8 mb-3">
						{ModuleInfo.map((moduleDetail, i) => {
							return (
								// set unique key per item via props
								<div key={i}>
									<Button
										className="togbtn mb-2"
										size="lg"
										color="info"
										block
										id={"toggler" + i} // use unique key/index to target correct toggle otherwise all work at once
									>
										{moduleDetail.Id_code} - {moduleDetail.Name}
									</Button>
									<UncontrolledCollapse
										toggler={"toggler" + i} //toggled container target via key/index
										className="mb-2"
									>
										<Card>
											<CardBody className="row justify-content-center">
												<ul className="list-group col-12 col-sm-8">
													<div>
														<li className="list-group-item d-flex justify-content-between">
															<strong>Academic Lead: </strong>
															{moduleDetail.Academic_id}
														</li>
														<li className="list-group-item d-flex justify-content-between">
															<strong>Learning Outcomes:</strong>
															{moduleDetail.Learning_outcomes + ""}
														</li>
														<li className="list-group-item d-flex justify-content-between">
															<strong>No. of hours: </strong>{" "}
															{moduleDetail.No_of_hours}
														</li>
														<li className="list-group-item d-flex justify-content-between">
															<strong>No. of credits: </strong>{" "}
															{moduleDetail.No_of_credits}
														</li>
														<li className="list-group-item d-flex justify-content-between">
															<strong>Assessments: </strong>{" "}
															{moduleDetail.Assessments + ""}
														</li>
													</div>
												</ul>
											</CardBody>
										</Card>
									</UncontrolledCollapse>
								</div>
							);
						})}
					</div>
				</div>
			</section>
		);
	}
}

export default Modules;
