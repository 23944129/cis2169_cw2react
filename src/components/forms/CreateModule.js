import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Input } from "reactstrap";

class CreateModule extends Component {
	render() {
		return (
			<section id="createModule" className="container mb-5">
				<div className="row justify-content-center">
					<div className="card col-12 col-md-8 text-center px-0 mb-4">
						<h3 className="card-header">Create Module</h3>
						<div className="card-body row justify-content-center">
							<form className="col-10 col-lg-8">
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Academic ID</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Module ID</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Module Name</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Number of Hours</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Number of Credits</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Learning Outcomes</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-3">
									<div className="input-group-prepend">
										<span className="input-group-text">Add Assessments</span>
									</div>

									<Input
										type="select"
										name="selectMulti"
										id="exampleSelectMulti"
										multiple
									>
										<option>A01 - Assessment 01</option>
										<option>A02 - Assessment 02</option>
										<option>A03 - Assessment 03</option>
										<option>A04 - Assessment 04</option>
										<option>A05 - Assessment 05</option>
										<option>A06 - Assessment 06</option>
										<option>A07 - Assessment 07</option>
										<option>A08 - Assessment 08</option>
										<option>A09 - Assessment 09</option>
										<option>A10 - Assessment 10</option>
										<option>A11 - Assessment 11</option>
										<option>A12 - Assessment 12</option>
									</Input>
								</div>
								<NavLink
									to="/modules"
									className="btn btn-info btn-block"
									type="submit"
								>
									Save Module
								</NavLink>
							</form>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default CreateModule;
