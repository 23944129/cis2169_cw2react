import React, { Component } from "react";
import { NavLink } from "react-router-dom";

class CreateAssessment extends Component {
	render() {
		return (
			<section id="createAssessment" className="container mb-5">
				<div className="row justify-content-center">
					<div className="card col-12 col-md-8 text-center px-0 mb-4">
						<h3 className="card-header">Create Assessment</h3>
						<div className="card-body row justify-content-center">
							<form className="col-10 col-lg-8">
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Module ID</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Title</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Number</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Learning Outcomes</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Volume</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Weighting</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-3">
									<div className="input-group-prepend">
										<span className="input-group-text">Submission Date</span>
									</div>
									<input type="datetime-local" className="form-control"></input>
								</div>

								<NavLink
									to="/assessments"
									className="btn btn-info btn-block"
									type="submit"
								>
									Save Assessment
								</NavLink>
							</form>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default CreateAssessment;
