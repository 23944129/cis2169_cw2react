import React, { Component } from "react";

class SignIn extends Component {
	render() {
		return (
			<section id="createDegree" className="container mb-5">
				<div className="row justify-content-center">
					<div className="card col-12 col-md-10 text-center px-0 mb-4">
						<h3 className="card-header">Sign In</h3>
						<div className="card-body row justify-content-center">
							<form className="col-10 col-md-8 col-lg-6">
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Staff ID</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Password</span>
									</div>
									<input type="password" className="form-control"></input>
								</div>
								<a
									className="btn btn-info btn-block my-2 my-sm-0"
									href="/"
									type="submit"
								>
									Sign In
								</a>
							</form>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default SignIn;
