import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Calendar from "react-calendar"; //import calender feature

class Schedule extends Component {
	render() {
		return (
			<section id="scheduleModule" className="container mb-5">
				<div className="row justify-content-center mb-4">
					<Calendar /> {/*Calender rendered from import */}
				</div>

				<div className="row justify-content-center mb-5">
					<div className="card col-12 col-md-8 text-center px-0">
						<h3 className="card-header">Schedule Module Time Slots</h3>
						<div className="card-body row justify-content-center">
							<form className="col-10 col-md-8 col-lg-6">
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Academic ID</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Module ID</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Room ID</span>
									</div>
									<input type="text" className="form-control"></input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Time slot</span>
									</div>
									<input type="time" className="form-control"></input>
								</div>

								<NavLink
									to="/modules"
									className="btn btn-info btn-block"
									type="submit"
								>
									Add to Calendar
								</NavLink>
							</form>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default Schedule;
