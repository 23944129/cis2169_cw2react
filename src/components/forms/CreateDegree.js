import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Form, Input } from "reactstrap";

class CreateDegree extends Component {
	render() {
		return (
			<section id="createDegree" className="container mb-5">
				<div className="row justify-content-center">
					<div className="card col-12 col-md-8 text-center px-0 mb-4">
						<h3 className="card-header">Create Degree Programme</h3>
						<div className="card-body row justify-content-center">
							<Form className="col-10 col-lg-8">
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">ID Code</span>
									</div>
									<Input type="text" className="form-control"></Input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Name</span>
									</div>
									<Input type="text" className="form-control"></Input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Learning Outcomes</span>
									</div>
									<Input type="text" className="form-control"></Input>
								</div>
								<div className="input-group mb-2">
									<div className="input-group-prepend">
										<span className="input-group-text">Exit Awards</span>
									</div>
									<Input type="text" className="form-control"></Input>
								</div>

								<div className="input-group mb-3">
									<div className="input-group-prepend">
										<span className="input-group-text">Add Modules</span>
									</div>

									<Input
										type="select"
										name="selectMulti"
										id="exampleSelectMulti"
										multiple
									>
										<option>M01 - Module 01</option>
										<option>M02 - Module 02</option>
										<option>M03 - Module 03</option>
										<option>M04 - Module 04</option>
									</Input>
								</div>

								<NavLink
									to="/degrees"
									className="btn btn-info btn-block"
									type="submit"
								>
									Save Degree Programme
								</NavLink>
							</Form>
						</div>
					</div>
				</div>
			</section>
		);
	}
}

export default CreateDegree;
