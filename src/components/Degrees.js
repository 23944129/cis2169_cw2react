import React, { Component } from "react";
import { UncontrolledCollapse, Button, CardBody, Card } from "reactstrap";
import DegreeInfo from "../data/degrees.json";

class Degrees extends Component {
	render() {
		return (
			<section id="viewDegrees" className="container mb-5">
				<div className="row justify-content-center">
					<h2 className="titles col-8 mb-3 text-center">Degree List</h2>
					<div className="col-8 mb-4">
						{DegreeInfo.map((degreeDetail, i) => {
							return (
								// set unique key per item via props
								<div key={i}>
									<Button
										className="togbtn mb-2"
										size="lg"
										color="info"
										block
										id={"toggler" + i} // use unique key/index to target correct toggle otherwise all work at once
									>
										{degreeDetail.Id_code} - {degreeDetail.Name}
									</Button>
									<UncontrolledCollapse
										toggler={"toggler" + i} //toggled container target via key/index
										className="mb-2"
									>
										<Card>
											<CardBody className="row justify-content-center">
												<ul className="list-group col-12 col-sm-8">
													<div>
														<li className="list-group-item d-flex justify-content-between">
															<strong>Learning Outcomes:</strong>
															{degreeDetail.Learning_outcomes + ""}
														</li>
														<li className="list-group-item d-flex justify-content-between">
															<strong>Exit Awards: </strong>
															{degreeDetail.Exit_awards + ""}
														</li>
														<li className="list-group-item d-flex justify-content-between">
															<strong>Modules: </strong>
															{degreeDetail.Modules + ""}
														</li>
														<li className="list-group-item d-flex justify-content-between">
															<strong>Assessments: </strong>
															{degreeDetail.Module_Assessments + ""}
														</li>
													</div>
												</ul>
											</CardBody>
										</Card>
									</UncontrolledCollapse>
								</div>
							);
						})}
					</div>
				</div>
			</section>
		);
	}
}

export default Degrees;
